/* global browser */

const copyIpAddress = () => {
  browser.tabs.query({ active: true, currentWindow: true }).then((res) => {
    const { url } = res[0];

    if (!url) return;

    const l = new URL(url);

    const a = document.createElement('input');
    a.value = l.hostname;
    a.style = { display: 'none' };
    document.body.appendChild(a);
    a.select();
    document.execCommand('Copy');
    document.body.removeChild(a);
  });
};

browser.browserAction.onClicked.addListener(copyIpAddress);
